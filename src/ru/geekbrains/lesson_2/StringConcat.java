package ru.geekbrains.lesson_2;

public class StringConcat {

    public static void main(String[] args) {
        //Пример объявления и конкатенации(сложения) строк
        String s1 = "Hello ";
        String s2 = "World";
        String s3 = "!!!";
        String s4 = s1 + s2 + s3;
        System.out.println("s1 = " + s1);
        System.out.println("s2 = " + s2);
        System.out.println("s3 = " + s3);
        System.out.println("s4 = " + s4);
    }
}
