package ru.geekbrains.lesson_2;

import java.util.Scanner;

public class OpSwitch {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите число от 1 до 3");
        final int a = sc.nextInt();
        //Демонстрация ветвления в зависимости от значения переменной a
        switch (a){
            case 1:
                System.out.println("Вы ввели число 1");
                break;
            case 2:
                System.out.println("Вы ввели число 2");
                break;
            case 3:
                System.out.println("Вы ввели число 3");
                break;
            default:
                System.out.println("Ошибка ввода");
        }
        sc.close();
        System.out.println("Программа завершена");
    }
}
