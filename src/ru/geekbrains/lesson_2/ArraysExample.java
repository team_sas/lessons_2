package ru.geekbrains.lesson_2;

import java.util.Arrays;

public class ArraysExample {

    public static void main(String[] args) {
        int[] arr1; //Объявляем переменную массива int
        arr1 = new int[5]; //Выделяем память оператором new под массив из 5 int и присваиваем ссылку на него переменной arr_1;
        arr1 = null; //Присваиваем ссылочному типу специальное значение null, которое обозначает, что ссылка ни куда не указывает
//        Если не осталось ни одной ссылки на существующий массив, то через некоторое время, сборщик мусора автоматически освободит память, которую выделяли под массив оператором new

        int[] arr2 = { 1, 1, 0, 0, 0, 1, 1, 0 }; //Быстрая инициализация массива, вожможна при объявнении новой переменной
        printArrayCycle(arr2); //Поэлементное чтение данных из массива с выводом в консоль
        System.out.println(Arrays.toString(arr2)); // Использование стандартного метода Arrays.toString() для преобразования одномерного массива в строку, с выводом её в консоль
        fillArray(arr2, 58);
        System.out.println(Arrays.toString(arr2));

        int[][] arr3 = new int[3][5]; //Объявление двумерного массива и выделение памяти под массив int 3 на 4;
        fillArray(arr3);
        printArrayCycle(arr3);
        System.out.println(Arrays.deepToString(arr3)); //Преобразование двумерного массива в строку и вывод её в консоль стандартным методом Arrays.deepToString()
        fillArray(arr3, 255);
        printArrayCycle(arr3);
        System.out.println(Arrays.deepToString(arr3)); //Преобразование двумерного массива в строку и вывод её в консоль стандартным методом Arrays.deepToString()


        int[][] arr4 = {{21,22,23}, {24,25,26}, {27,28,29}}; //Быстрая инициализация двумерного массива
        printArrayCycle(arr4);
    }

    /**
     * Примеры заполнения массивов в циклах, и сразу демострация перегрузки методов
     */
    //Заполнение всех ячеек одномерного массива числом value
    private static void fillArray(int[] arr, int value){ for(int i=0; i<arr.length; i++) arr[i] = value; }

    //Заполнения всех ячеек двумерного массива числом value
    private static void fillArray(int[][] arr, int value){
        for(int i=0; i<arr.length; i++){
            for(int j=0; j<arr[i].length; j++) arr[i][j] = value;
        }
    }
    //Заполнения двумерного массива числами по порядку от 1 до конца массива
    private static void fillArray(int[][] arr){
        int cellValue = 1;
        final int firstDimLength = arr.length;
        for(int i=0; i<firstDimLength; i++){
            final int secondDimLength = arr[i].length;
            for(int j=0; j<secondDimLength; j++){
                arr[i][j] = cellValue;
                cellValue++;
            }
        }
    }

    /**
     * Далее примеры вывода массивов в консоль, и тут опять же перегруз методов
     */
    // Поэлементное считывание одномерного массива и вывод его в консоль
    private static void printArrayCycle(int[] arr){
        for(int i=0; i<arr.length; i++) System.out.print(arr[i] + " ");
        System.out.println();
    }

    //Поэлементное считывание двумерного массива и вывод результата в консоль
    private static void printArrayCycle(int[][] arr){
        for(int i=0; i<arr.length; i++){
            for(int j=0; j<arr[i].length; j++) System.out.print(arr[i][j] + " ");
        }
        System.out.println();
    }
}
