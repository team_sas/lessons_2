package ru.geekbrains.lesson_2;

public class Cycles {

    public static void main(String[] args) {
//        Пример простого цикла while с предусловием
        simpleWhilePreCycle();
//        Пример простого цикла for с предусловием
        simpleForCycle();
//        Привет простого цикла while с постусловием
        simpleWhilePostCycle();
//        Демонстрация прерывания цикла оператором break
        demoBreak();
//        Демонстрация пропуска итерации оператором continue;
        demoContinue();
//        Далее еще несколько примеров работы с циклами
        for (int i = 0; i < 3; i++) {
            System.out.println("i: " + i);
        }
        System.out.println("---------------------------");
        for (int i = 10; i > 2; i -= 2) {
            System.out.println("i: " + i);
        }
        System.out.println("---------------------------");
        for (int i = 0; i < 8; i++) {
            if (i == 4) break;
            System.out.println("i: " + i);
        }
        System.out.println("---------------------------");
        int[] a = new int[5];
        for (int i = 0; i < 5; i++) {
            a[i] = i * 3;
            System.out.print(a[i] + " ");
        }
        System.out.println();
        System.out.println("---------------------------");
        int b[] = {1, 6, 3, 1, 1};
        for (int i = 0; i < b.length; i++) {
            if (b[i] < 4) b[i] *= 2;
            else b[i] *= 3;
            System.out.print(b[i] + " ");
        }
        System.out.println();
        System.out.println("---------------------------");
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.println("i: " + i + " j: " + j);
            }
        }
        System.out.println("---------------------------");
        // печатаем таблицу 3х3, стостоящую из *
        for (int i = 0; i < 3; i++) { // отвечает за перевод строк
            for (int j = 0; j < 3; j++) { // отвечает за печать строки
                System.out.print("* "); // печатаем * и пробел
            }
            System.out.println(); // переводим строку
        }
    }

    private static void simpleWhilePreCycle() {
        //Выводим 10 раз подряд строчку с текущим значением переменной i
        System.out.println("Демонстрация простого цикла while с предусловием");
        int i = 0;
        while (i < 10) {
            System.out.println("Значение i = " + i);
            i++;
        }
        System.out.println("Цикл завершён.");
    }

    private static void simpleForCycle() {
        System.out.println("Демонстрация простого цикла for с предусловием");
        for (int i = 0; i < 10; i++) System.out.println("Значение i = " + i);
        System.out.println("Цикл завершён.");
    }

    private static void simpleWhilePostCycle() {
        System.out.println("Демонстрация простого цикла while с постусловием");
        int i = 0;
        do {
            System.out.println("Значение i = " + i);
            i++;
        } while (i < 10);
        System.out.println("Цикл завершён.");
    }

    private static void demoBreak() {
        System.out.println("Демонстрация прерывания цикла оператором break");
        for (int i = 0; i < 10; i++) {
            System.out.println("Значение i = " + i);
            if (i == 5) {
                System.out.println("Цикл прерван при i == 5");
                break;
            }
        }
    }

    private static void demoContinue() {
        System.out.println("Демонстрация пропуска итерации цикла оператором continue");
        for (int i = 0; i < 10; i++) {
            if (i == 5) continue;
            System.out.println("Значение i = " + i);
        }
    }
}
