package ru.geekbrains.lesson_2;

import java.util.Scanner;

public class ConsoleInput {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        //В конце каждого ввода нажимаем Enter
        System.out.println("Введите одно слово...");
        String word = sc.next();
        System.out.println("Ваше введённое слово: '" + word + "'");
        System.out.println("Введите одно целое число...");
        int a = sc.nextInt();
        System.out.println("Вы ввели число: '" + a + "'");
        System.out.println("Введите два целых числа через пробел...");
        a = sc.nextInt();
        int b = sc.nextInt();
        System.out.println("Вы ввели числа " + a + " и " + b);
        System.out.println("Введите данные, будет считана полностью введённая строка...");
        sc.nextLine(); // Если это не написать, то следующая строка получит из консоли конец предыдущей строки, с двумя int
        String line = sc.nextLine();
        System.out.println("Вы ввели строку: '" + line + "'");
        sc.close();
    }
}
